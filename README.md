categulario/hugo
================

`categulario/hugo` is a [Docker](https://www.docker.io) base image for static
sites generated with [Hugo](http://gohugo.io). Based on [this
image](https://github.com/oskapt/docker-hugo).

This image is uses the `extended` release of Hugo, which contains support for
Sass/SCSS.
